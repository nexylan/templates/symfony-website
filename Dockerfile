FROM composer:1.9 as composer
FROM registry.gitlab.com/nexylan/docker/core:1.10.0 as core

FROM php:7.4-alpine3.10 as init
COPY --from=core / /
RUN setup
COPY --from=composer /usr/bin/composer /usr/bin/composer
EXPOSE 9999
ENV IMAGE_VERSION dev
RUN docker-php-ext-install -j"$(nproc)" pdo pdo_mysql
ARG APP_ENV
COPY rootfs /
ARG USER
RUN /user.sh
RUN mkdir /app
RUN chown ${USER} /app
USER ${USER}
WORKDIR /app

FROM init as dev
ENTRYPOINT [ "/entrypoint.sh" ]

FROM init as prod
COPY bin bin
COPY config config
COPY public public
COPY src src
COPY templates templates
COPY composer.json .
COPY composer.lock .
COPY symfony.lock .
COPY .env .
RUN /entrypoint.sh

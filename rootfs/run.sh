#!/usr/bin/env sh
set -e

console="bin/console --no-interaction"
wait-for-it --timeout=0 db:3306
${console} doctrine:database:create --if-not-exists
# ${console} doctrine:migrations:migrate

php --server 0.0.0.0:9999 -t public

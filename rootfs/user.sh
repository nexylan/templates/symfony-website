#!/usr/bin/env sh

if [ -n "${USER}" ] && [ "${USER}" != "root" ]; then
	echo "Create user: ${USER}"
	adduser \
		--disabled-password \
		--home="/home/${USER}" \
		--shell /bin/sh \
		"${USER}"
fi

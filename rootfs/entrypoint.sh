#!/usr/bin/env sh
set -e

if [ "${APP_ENV}" = "prod" ]; then
	COMPOSER_INSTALL_FLAGS="--no-dev"
fi

echo "APP_ENV: ${APP_ENV}"
echo "COMPOSER_INSTALL_FLAGS: ${COMPOSER_INSTALL_FLAGS}"

composer install ${COMPOSER_INSTALL_FLAGS}

exec "$@"

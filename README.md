# Symfony website

A fancy symfony website.

🚨 NOT READY FOR PRODUCTION, yet. 🚨

Any contribution is welcome! 👍

## Requirements

- Docker

## New project setup

Get the download link of the latest version from the [releases page](-/releases).

```bash
wget https://gitlab.com/nexylan/templates/symfony-website/-/archive/master/symfony-website-master.tar.gz
tar xf symfony-website-master.tar.gz
rm symfony-website-master.tar.gz
mv symfony-website-master my-project
cd my-project
git init .
```

## Usage

Run:

```
make
```

Then click on the given url at the end on the output.

And voilà, you are ready to rock. 🎸

## Deployment

See: https://gitlab.com/nexylan/templates/docs/tree/v1.0.0#deployment

COMPOSE = docker-compose \
	--file docker-compose.dev.yml \
	--file docker-compose.yml

all: build up ps

build:
	@$(COMPOSE) build --parallel

up:
	@$(COMPOSE) up --remove-orphans --detach

ps:
	@$(COMPOSE) ps
	@echo http://localhost:`docker-compose port app 9999 | cut -d : -f 2`

test:
	@$(COMPOSE) run test

clean:
	@$(COMPOSE) down
	@git clean -fdX
